import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:prueba_pragma/UI/screens/home_screen.dart';
import 'package:prueba_pragma/core/cubit/cats_cubit.dart';
import 'package:prueba_pragma/core/data/network/api_cat.dart';
import 'package:prueba_pragma/routes.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(375, 812),
        builder: (context, child) => MultiBlocProvider(
              providers: [
                BlocProvider<CatsCubit>(
                  create: (context) => CatsCubit(api: ApiCat()),
                )
              ],
              child: MaterialApp(
                debugShowCheckedModeBanner: false,
                onGenerateRoute: Routes.generateRoutes,
                home: AnimatedSplashScreen(
                  splash: 'assets/cat_splash.png',
                  splashIconSize: 250.h,
                  nextScreen: const HomeScreen(),
                  centered: true,
                  splashTransition: SplashTransition.sizeTransition,
                ),
              ),
            ));
  }
}
