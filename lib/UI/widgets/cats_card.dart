import 'package:flutter/material.dart';
import 'package:prueba_pragma/UI/styles/app_styles.dart';
import 'package:prueba_pragma/core/models/cat_model.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CatsCard extends StatelessWidget {
  final Cat cat;
  const CatsCard({Key? key, required this.cat}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5.h),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 15.h),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(cat.name!, style: AppStyles.title2),
                Text('Mas...', style: AppStyles.title2)
              ],
            ),
          ),
          Container(
            height: 250.h,
            width: cat.image!.width!.toDouble(),
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(30)),
            child: Image.network(
              cat.image!.url!,
              fit: BoxFit.fill,
              loadingBuilder: (context, child, loading) {
                if (loading == null) return child;
                return Center(
                  child: CircularProgressIndicator(
                    color: Colors.purple,
                    backgroundColor: Colors.orange.shade300,
                    value: loading.expectedTotalBytes != null
                        ? loading.cumulativeBytesLoaded /
                            loading.expectedTotalBytes!
                        : null,
                  ),
                );
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 28.h),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Text(
                    'Pais de origen: ${cat.origin!}',
                    style: AppStyles.title2,
                    overflow:
                        cat.origin!.length > 12 ? TextOverflow.ellipsis : null,
                  ),
                ),
                Text('Edad: ${cat.intelligence!}', style: AppStyles.title2)
              ],
            ),
          ),
        ],
      ),
    );
  }
}
