import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'app_styles.dart';

class TitleAndContent extends StatelessWidget {
  final String title;
  final String value;
  const TitleAndContent({
    Key? key,
    required this.title,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
        text: TextSpan(children: [
      TextSpan(text: title, style: AppStyles.titleBold1),
      TextSpan(
          text: value, style: AppStyles.title1.copyWith(color: Colors.black))
    ]));
  }
}
