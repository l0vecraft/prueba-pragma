import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppStyles {
  static TextStyle title1 = TextStyle(fontSize: 22.sp);
  static TextStyle title2 = TextStyle(fontSize: 18.sp);
  static TextStyle titleBold1 = TextStyle(
      color: Colors.black, fontSize: 20.sp, fontWeight: FontWeight.bold);
}
