import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prueba_pragma/UI/screens/detail_screen.dart';
import 'package:prueba_pragma/UI/widgets/cats_card.dart';
import 'package:prueba_pragma/UI/widgets/custom_loading.dart';
import 'package:prueba_pragma/core/cubit/cats_cubit.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Catbreeds'),
        centerTitle: true,
        backgroundColor: Colors.purple,
      ),
      body: BlocConsumer<CatsCubit, CatsState>(
        listener: (context, state) {},
        builder: (context, state) {
          if (state is CatsLoaded) {
            return Container(
                padding: EdgeInsets.only(top: 18.h),
                child: ListView.separated(
                    separatorBuilder: (context, index) =>
                        SizedBox(height: 20.h),
                    itemCount: state.listOfCats.length,
                    padding: EdgeInsets.symmetric(horizontal: 10.w),
                    itemBuilder: (context, index) => OpenContainer(
                        openElevation: 3,
                        closedElevation: 6,
                        closedBuilder: (context, action) =>
                            CatsCard(cat: state.listOfCats[index]),
                        openBuilder: (context, returnValue) => DetailScreen(
                              cat: state.listOfCats[index],
                            ))));
          } else {
            return const CustomLoading();
          }
        },
      ),
    );
  }
}
