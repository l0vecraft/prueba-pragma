import 'package:flutter/material.dart';
import 'package:prueba_pragma/UI/styles/title_and_content.dart';
import 'package:prueba_pragma/core/models/cat_model.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DetailScreen extends StatefulWidget {
  final Cat cat;
  const DetailScreen({Key? key, required this.cat}) : super(key: key);

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Catbreeds'),
          centerTitle: true,
          backgroundColor: Colors.purple,
        ),
        body: Container(
          child: Column(
            children: [
              Expanded(
                  child: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(widget.cat.image!.url!),
                        fit: BoxFit.cover)),
              )),
              Expanded(
                  child: Scrollbar(
                thickness: 8,
                child: ListView(
                  children: [
                    Container(
                        padding: EdgeInsets.symmetric(horizontal: 10.w),
                        child: Text(
                          widget.cat.description!,
                          style: TextStyle(fontSize: 20.sp),
                        )),
                    Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 10.w, vertical: 5.h),
                        child: TitleAndContent(
                            title: 'Inteligencia: ',
                            value: widget.cat.intelligence!.toString())),
                    Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 10.w, vertical: 5.h),
                        child: TitleAndContent(
                            title: 'Adaptabilidad: ',
                            value: widget.cat.adaptability!.toString())),
                    Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 10.w, vertical: 5.h),
                        child: TitleAndContent(
                            title: 'Nivel de energia: ',
                            value: widget.cat.energyLevel!.toString())),
                    Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 10.w, vertical: 5.h),
                        child: TitleAndContent(
                            title: 'Nivel de amistad con perros: ',
                            value: widget.cat.dogFriendly!.toString())),
                    Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 10.w, vertical: 2.h),
                        child: TitleAndContent(
                            title: 'Tiene pelo: ',
                            value: widget.cat.hairless! == 0 ? "Si" : "No")),
                    Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 10.w, vertical: 2.h),
                        child: TitleAndContent(
                            title: 'Hipoalergenico: ',
                            value:
                                widget.cat.hypoallergenic! == 0 ? "Si" : "No")),
                  ],
                ),
              ))
            ],
          ),
        ));
  }
}
