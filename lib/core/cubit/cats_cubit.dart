import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:prueba_pragma/core/data/repository/cat_repository.dart';
import 'package:prueba_pragma/core/expcetions/Failure.dart';
import 'package:prueba_pragma/core/models/cat_model.dart';

part 'cats_state.dart';

class CatsCubit extends Cubit<CatsState> {
  final CatRepository api;
  CatsCubit({required this.api}) : super(CatsInitial()) {
    getData();
  }

  /// It emits a CatsLoading event, then it tries to get the data from the api. If it succeeds, it emits
  /// a CatsLoaded event, otherwise it emits a CatsError event.
  void getData() async {
    emit(CatsLoading());
    try {
      var res = await api.getDataCats();
      emit(CatsLoaded(listOfCats: res));
    } on Failure catch (e) {
      emit(CatsError(exception: e.title));
    }
  }
}
