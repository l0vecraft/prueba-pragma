part of 'cats_cubit.dart';

abstract class CatsState extends Equatable {
  const CatsState();

  @override
  List<Object> get props => [];
}

class CatsInitial extends CatsState {}

class CatsLoading extends CatsState {}

class CatsLoaded extends CatsState {
  final List<Cat> listOfCats;
  const CatsLoaded({required this.listOfCats});
}

class CatsError extends CatsState {
  final String exception;
  const CatsError({required this.exception});
}
