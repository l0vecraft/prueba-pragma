import 'package:prueba_pragma/core/models/cat_model.dart';

abstract class CatRepository {
  Future<List<Cat>> getDataCats();
}
