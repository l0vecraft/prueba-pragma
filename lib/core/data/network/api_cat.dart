import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:prueba_pragma/core/data/repository/cat_repository.dart';
import 'package:prueba_pragma/core/expcetions/Failure.dart';
import 'package:prueba_pragma/core/models/cat_model.dart';

class ApiCat implements CatRepository {
  final _dio = Dio(BaseOptions(
      headers: {'x-api-key': 'bda53789-d59e-46cd-9bc4-2936630fde39'},
      baseUrl: 'https://api.thecatapi.com/v1/'));
  @override
  Future<List<Cat>> getDataCats() async {
    List<Cat> listOfCats = [];
    try {
      Response result = await _dio.get('breeds');
      if (result.data != '') {
        result.data.forEach((e) {
          listOfCats.add(Cat.fromJson(e));
        });
      }
    } on Exception {
      throw Failure(title: 'Error al obtener los datos');
    }
    return listOfCats;
  }
}
